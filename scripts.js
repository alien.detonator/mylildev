var menuStatus = 0;

	function openNav() {
		if(menuStatus===0){
		  document.getElementById("myNav").style.width = "80%";
		  menuStatus =1;
	}else{
		document.getElementById("myNav").style.width = "0%";
		 menuStatus =0;
	}		  
		}


function closeNav() {
  document.getElementById("myNav").style.width = "0%";
}

function input_notEmpty(){
	var empty=true;
	var fname = document.getElementById("first_name").value;
	var lname = document.getElementById("last_name").value;
	var uname = document.getElementById("username").value;
	var email = document.getElementById("email").value;
	var pw1 = document.getElementById("password1").value;
	var pw2 = document.getElementById("password2").value;


		if(fname.length === 0){
			document.getElementById("first_name").className="input_error";
			document.getElementById("empty").innerHTML = "A * jelölt mezők kötelezőek!";
			empty=true;
		}else{
			empty=false;
		}

		if(lname.length === 0){
			document.getElementById("last_name").className="input_error";
			document.getElementById("empty").innerHTML = "A * jelölt mezők kötelezőek!";
			empty=true;
		}else{
			empty=false;
		}

		if(uname.length === 0){
			document.getElementById("username").className="input_error";
			document.getElementById("empty").innerHTML = "A * jelölt mezők kötelezőek!";
			empty=true;
		}else{
			empty=false;
		}

		if(email.length === 0){
			document.getElementById("email").className="input_error";
			document.getElementById("empty").innerHTML = "A * jelölt mezők kötelezőek!";
			empty=true;
		}else{
			empty=false;
		}

		if(pw1.length === 0){
			document.getElementById("password1").className="input_error";
			document.getElementById("empty").innerHTML = "A * jelölt mezők kötelezőek!";
			empty=true;
		}else{
			empty=false;
		}

		if(pw2.length === 0){
			document.getElementById("password2").className="input_error";
			document.getElementById("empty").innerHTML = "A * jelölt mezők kötelezőek!";
			empty=true;
		}else{
			empty=false;
		}

		if(document.getElementById("woman").checked === false && document.getElementById("man").checked === false ){
			document.getElementById("sex2").className="input_error_sex";
			document.getElementById("empty").innerHTML = "A * jelölt mezők kötelezőek!";
			empty=true;
		}else{
			empty=false;
		}

		password_check(empty);
}

function password_check(empty){ 
	if(empty === false){	//csak akkor fut ha minden mező ki van töltve

		var pw1 = document.getElementById("password1").value;
		var pw2 = document.getElementById("password2").value;
		
		if(pw1 !== pw2 || pw1.length === 0 || pw2.length === 0 ){

			document.getElementById("password1").className="input_error";
			document.getElementById("password2").className="input_error";
			document.getElementById("notMatchPassw").innerHTML = "Nem egyezik meg a két jelszó!";
		}else{
			document.getElementById("submit").type="submit";
			document.getElementById("submit").id="submit_ready";
		}
	}
}

function login_empty(){
	var empty_uname=true;
	var empty_pass=true;
	var unameLog = document.getElementById("userLogin").value;
	var pwLog = document.getElementById("passwordLogin").value;


	if(unameLog.length === 0){
		document.getElementById("emptyUname").innerHTML = "Adj meg egy felhasználónevet!";
		document.getElementById("userLogin").className="input_error";

		empty_uname=true;
	}else{
		empty_uname=false;
	}

	if(pwLog.length === 0){
		document.getElementById("emptyPass").innerHTML = "Adj meg egy jelszót!";
		document.getElementById("passwordLogin").className="input_error";

		empty_pass=true;
	}else{
		empty_pass=false;
	}

	if(empty_uname === false && empty_pass === false){
		document.getElementById("submit_login").type="submit";
		document.getElementById("submit_login").id="submit_ready";
	}

}

function zoomin(){
	document.getElementById("zoom").className="zoom_in";
}

function zoomout(){
	document.getElementById("zoom").className="card";
}